DELIMITER $$
CREATE FUNCTION `GENERATE_ID`(length SMALLINT(3)) RETURNS varchar(100) CHARSET utf8
BEGIN 
	SET @returnStr = '';
		SET @allowedChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		SET @i = 0;
		WHILE (@i < length) DO
				SET @returnStr = CONCAT(@returnStr, substring(@allowedChars, FLOOR(RAND() * LENGTH(@allowedChars) + 1), 1));
				SET @i = @i + 1;
		END WHILE;

		RETURN @returnStr;
END$$
DELIMITER ;



DELIMITER $$
CREATE TRIGGER preinsert_testtable
	BEFORE INSERT ON `testtable`
	FOR EACH ROW
	BEGIN
		SET @id = GENERATE_PK(11);
		WHILE (@id IS NOT NULL) DO 
			SET NEW.id = @id;
			SET @id = (SELECT id FROM `testtable` WHERE `id` = NEW.id);
		END WHILE;
	END;$$
DELIMITER ;

SET @pkvalue = (SELECT id FROM `testtable` WHERE `id`='3')
RETURN CASE(@pkvalue WHEN NULL THEN GENERATE_PK(11) ELSE @pkvalue END)


SET @pkvalue = (SELECT id FROM `testtable` WHERE `id`='2');
SELECT IFNULL(@pkvalue, GENERATE_PK(11)) AS id;

SET @newid = GENERATE_PK(11);
SET @existing = (SELECT COUNT(id) FROM `testtable` WHERE `id`=@newid);
SET @id = (CASE @existing WHEN 0 THEN @newid ELSE GENERATE_PK(11) END);
SELECT @newid AS newid, @existing AS existing, @id AS id;


ngo_lists_donor_relationships