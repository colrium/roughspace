package net.equus.com.app;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.equus.net.libraries.app.Notifacation;
import com.equus.net.libraries.app.logindialog.LoginDialog;
import com.equus.net.libraries.app.sweetalertdialog.SweetAlertDialog;
import com.equus.net.libraries.helpers.DeviceUtils;
import com.equus.net.libraries.helpers.FontManager;
import com.equus.net.libraries.helpers.HttpHelper;
import com.equus.net.libraries.helpers.SyncUtilities;
import com.equus.net.libraries.material.rey.app.ProgressDialog;
import com.equus.net.libraries.material.rey.drawable.GoogleIconDrawable;
import com.equus.net.libraries.rfid.RFIDHelper;

import net.equus.com.app.modules.system.DashboardActivity;
import net.equus.com.app.utilities.SessionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final int SPLASH_STARTUP_DELAY = 300;
    public static final int SPLASH_ANIM_ITEM_DURATION = 1000;
    public static final int SPLASH_ITEM_DELAY = 300;

    private boolean splashanimationStarted = false;

    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    SwipeRefreshLayout swipeRefreshLayout;
    View drawerheader;


    ViewFlipper vf_container;
    ScrollView scrollView;
    LinearLayout container;
    HttpHelper httpHelper;
    SessionHelper sessionHelper;

    boolean oncreatecalled = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SplashscreenTheme);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vf_container = (ViewFlipper) findViewById(R.id.vf_container);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus || splashanimationStarted) {
            return;
        }

        splashscreenanimations();
        super.onWindowFocusChanged(hasFocus);
    }



    private void splashscreenanimations() {
        FrameLayout fl_container = (FrameLayout) findViewById(R.id.fl_container);
        ImageView iv_appicon = (ImageView) findViewById(R.id.iv_appicon);
        TextView tv_appname = (TextView) findViewById(R.id.tv_appname);
        TextView tv_credit = (TextView) findViewById(R.id.tv_credit);
        sessionHelper = getPreferences();
        int[] appcolors = sessionHelper.getColors();

        fl_container.setBackgroundColor(appcolors[0]);
        iv_appicon.setImageBitmap(sessionHelper.getAppIcon());
        tv_appname.setText(sessionHelper.getAppName());

        ViewCompat.animate(iv_appicon)
                .translationY(-20)
                .setStartDelay(SPLASH_STARTUP_DELAY)
                .setDuration(SPLASH_ANIM_ITEM_DURATION)
                .setInterpolator(new DecelerateInterpolator(1.2f))
                .start();

        ViewCompat.animate(tv_appname)
                .alpha(1)
                .setStartDelay(500)
                .setDuration(500)
                .setInterpolator(new DecelerateInterpolator())
                .start();
        ViewCompat.animate(tv_credit)
                .alpha(1)
                .setStartDelay(SPLASH_ITEM_DELAY + 500)
                .setDuration(500)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        splashanimationStarted = true;
                        aftersplashscreen();
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                })
                .start();


    }

    private void aftersplashscreen(){
        setTheme(R.style.AppTheme_NoActionBar);
        vf_container.showNext();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        if (getIntent().getBooleanExtra("EXIT", false)){
            finish();
            return;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        swipeRefreshLayout  = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        container = (LinearLayout) findViewById(R.id.container);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawerheader = navigationView.getHeaderView(0);

        initialize();
    }

    public void initialize(){

        toolbar.setSubtitle("Home");
        swipeRefreshLayout.setColorSchemeColors(new int[]{appcolors[0], appcolors[2], appcolors[1]});
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                syncmodules();
            }
        });
        if (!sessionHelper.initsyncexecuted()){
            syncmodules();
        }
        else{
            initializemodules();
        }
        powerrfid(true);




    }




    public void powerrfid(boolean power){
        if (sessionHelper.autopowerrfid() && DeviceUtils.hasRFID()){

            ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMode(ProgressDialog.Mode.INDETERMINATE);
            progressDialog.setType(ProgressDialog.Type.CIRCULAR);
            if (power){
                progressDialog.setProgressTaskName("Starting RFID");
            }
            else{
                progressDialog.setProgressTaskName("Stopping RFID");
            }
            progressDialog.show();
            RFIDHelper rfidHelper = new RFIDHelper(MainActivity.this);

            rfidHelper.setOnRFIDListener(new RFIDHelper.RFIDListener() {
                @Override
                public void onOpen() {
                    if (power){
                        progressDialog.dismissImmediately();
                        Notifacation.info(MainActivity.this, "RFID Component Powered On!", Toast.LENGTH_LONG, new GoogleIconDrawable(MainActivity.this, R.string.settings_remote, Notifacation.INFO_COLOR, Notifacation.ICON_SIZE)).show();
                    }

                }

                @Override
                public void onInventoryStart() {

                }

                @Override
                public void onScan(String rfid, int totaldiscovered) {

                }

                @Override
                public void onMultiscan(String rfid, int count) {

                }

                @Override
                public void onInventoryStop(List<String> rfids) {

                }

                @Override
                public void onClose() {
                    if (!power){
                        progressDialog.dismissImmediately();
                        Notifacation.info(MainActivity.this, "RFID Component Powered Off!", Toast.LENGTH_LONG, new GoogleIconDrawable(MainActivity.this, R.string.settings_remote, Notifacation.INFO_COLOR, Notifacation.ICON_SIZE)).show();
                    }
                }
            });
            if (power){
                rfidHelper.stopRFID();
                rfidHelper.openRFID();
            }
            else{
                rfidHelper.stopRFID();
            }

        }
    }



    @Override
    protected void onResume(){
        super.onResume();
    }


    public void initializemodules() {
        container.removeAllViews();

        String packagename = getPackageName();
        int iconsize = 72;
        int textsize = 12;

        JSONObject modulesjson = sessionHelper.getActiveModules();
        List<String> modules = Arrays.asList(AppConstants.MODULES);
        List<String> definedmodules = Arrays.asList(AppConstants.MODULES);

        LinearLayout rowtemplayout = new LinearLayout(this);
        rowtemplayout.setOrientation(LinearLayout.HORIZONTAL);
        rowtemplayout.setPadding(10,10,10,10);

        LinearLayout.LayoutParams rowtemplayoutlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
        rowtemplayoutlp.setMargins(0,0,0, iconsize/2);
        rowtemplayout.setLayoutParams(rowtemplayoutlp);


        if (modulesjson.has("active")){
            modules = new ArrayList<>();
            List<String> modulelist = Arrays.asList(AppConstants.MODULES);
            try {
                JSONObject modulesdata = modulesjson.getJSONObject("active");
                JSONObject modulesicons = modulesjson.getJSONObject("icons");
                JSONObject modulescolors = modulesjson.getJSONObject("colors");
                Iterator iterator = modulesdata.keys();
                while(iterator.hasNext()){
                    String key = iterator.next().toString();
                    modules.add(key);
                }

                int rowitemscount = 0;
                for (int i=0; i < modules.size(); i++){
                    if (rowitemscount==0){
                        rowtemplayout = new LinearLayout(this);
                        rowtemplayout.setOrientation(LinearLayout.HORIZONTAL);
                        rowtemplayout.setPadding(10,10,10,10);
                        rowtemplayout.setLayoutParams(rowtemplayoutlp);
                    }
                    String module = modules.get(i);
                    String modulename = modulesdata.getString(module);
                    String moduleicon = modulesicons.getString(module);
                    String modulecolor = modulescolors.getString(module);
                    String classname = packagename+".modules."+module+".DashboardActivity";

                    com.equus.net.libraries.material.rey.widget.LinearLayout modulebtnlayout = new com.equus.net.libraries.material.rey.widget.LinearLayout(this);
                    modulebtnlayout.setOrientation(LinearLayout.VERTICAL);
                    modulebtnlayout.setPadding(5,5,5,5);
                    modulebtnlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                    modulebtnlayout.applyStyle(R.style.Material_Drawable_Ripple_Wave);

                    TextView modicontv = new TextView(this);
                    TextView modnametv = new TextView(this);


                    modicontv.setTextSize(iconsize);
                    modnametv.setTextSize(textsize);

                    int modiconid = getResources().getIdentifier(moduleicon, "string", packagename);
                    modicontv.setText(getResources().getString(modiconid));
                    modnametv.setText(modulename);

                    modicontv.setTextColor(getResources().getColor(R.color.grey_lighten_1));
                    modnametv.setTextColor(getResources().getColor(R.color.grey_lighten_1));

                    modicontv.setTypeface(FontManager.getTypeface(this, FontManager.MATERIALICONS));
                    modnametv.setTypeface(FontManager.getTypeface(this, FontManager.QUESTRIAL));

                    modicontv.setGravity(Gravity.CENTER);
                    modnametv.setGravity(Gravity.CENTER);

                    if (definedmodules.contains(module)){
                        modicontv.setTextColor(Color.parseColor(modulecolor));
                        modnametv.setTextColor(Color.parseColor(modulecolor));
                    }


                    modulebtnlayout.addView(modicontv);
                    modulebtnlayout.addView(modnametv);



                    try {
                        final Class<?> moduleclass = Class.forName(classname);
                        modulebtnlayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent moduleActivity = new Intent(MainActivity.this, moduleclass);
                                startActivity(moduleActivity);
                            }
                        });
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }


                    rowtemplayout.addView(modulebtnlayout);

                    if (rowitemscount==1){
                        rowitemscount = 0;
                        container.addView(rowtemplayout);
                    }
                    else{
                        rowitemscount++;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else{

        }
    }


    private void syncmodules(){
        HttpHelper.HttpListener httpListener = new HttpHelper.HttpListener() {
            @Override
            public void onStart() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                });
            }

            @Override
            public void onImageReady(Bitmap response) {
                sessionHelper.setAppIcon(response);
                sessionHelper = new SessionHelper(MainActivity.this);
                shortcutAdd();
            }

            @Override
            public void onJsonReady(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String appname = jsonObject.getString("name");
                    shortcutDel(sessionHelper.getAppName());
                    sessionHelper.setAppName(appname);
                    JSONObject colors = jsonObject.getJSONObject("colors");
                    int colorprimary = Color.parseColor(colors.getString("colorPrimary"));
                    int colorprimarydark = Color.parseColor(colors.getString("colorPrimaryDark"));
                    int coloraccent = Color.parseColor(colors.getString("colorAccent"));
                    sessionHelper.setColors(colorprimary, colorprimarydark, coloraccent);
                    JSONObject modulesjson = jsonObject.getJSONObject("modules");
                    sessionHelper.setActiveModules(modulesjson.toString());

                    if (!sessionHelper.initsyncexecuted()){
                        sessionHelper.setinitsyncexecuted(true);
                    }
                    applycustomization();
                    initializemodules();
                } catch (JSONException e) {
                    Log.wtf("JSON Error", e.toString());
                }

            }

            @Override
            public void onError(String errormsg) {
                Log.wtf("volleyerror", errormsg);
            }

            @Override
            public void onComplete() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        };
        httpHelper = new HttpHelper(MainActivity.this, httpListener);
        httpHelper.getJSONdata(SyncUtilities.fullurl(this, "config"));
        httpHelper.fetchimage(SyncUtilities.fullurl(this, "icon"));
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Close Application?")
                    .setContentText("Are you sure you want to Close Application?")
                    .setCancelText("Yes Close")
                    .setConfirmText("No")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismiss();
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismiss();
                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                        }
                    }).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setProgressTaskName("Logging In");
            progressDialog.setMode(ProgressDialog.Mode.INDETERMINATE);
            progressDialog.setType(ProgressDialog.Type.CIRCULAR);
            LoginDialog loginDialog = new LoginDialog(MainActivity.this);
            loginDialog.setOnSessionChangeListener(new LoginDialog.OnSessionChangeListener() {
                @Override
                public void onLoginStart() {
                    loginDialog.dismissImmediately();
                    progressDialog.show();
                }

                @Override
                public void onLogin(JSONObject userdetails) {
                    item.setTitle("Account");
                    sessionHelper = new SessionHelper(MainActivity.this);
                    progressDialog.dismissImmediately();
                    loginDialog.show();
                }

                @Override
                public void onLoginFailed(String error) {
                    progressDialog.dismissImmediately();
                    loginDialog.show();
                }

                @Override
                public void onLogout() {
                    sessionHelper = new SessionHelper(MainActivity.this);
                    item.setTitle("Login");
                }
            });
            loginDialog.show();
        }
        else if (id == R.id.nav_directories) {

            Intent directoriesActivity = new Intent(MainActivity.this, DashboardActivity.class);
            startActivity(directoriesActivity);
        }
        else if (id == R.id.nav_settings) {
            Intent settingsActivity = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsActivity);
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void shortcutAdd() {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(getApplicationContext(), MainActivity.class);
        shortcutIntent.setAction(Intent.ACTION_RUN);
        // Create bitmap with number in it -> very default. You probably want to give it a more stylish look
        Bitmap bitmap = sessionHelper.getAppIcon();
        String name = sessionHelper.getAppName();
        if (bitmap != null){
            Log.wtf("bitmap", "bitmap");
            ImageView imageView =  ((ImageView) findViewById(R.id.icon));
            if (imageView != null){
                imageView.setImageBitmap(bitmap);
            }
        }
        else{
            Log.wtf("bitmap", "nullbitmap");
        }



        // Decorate the shortcut
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);

        // Inform launcher to create shortcut
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }

    private void shortcutDel(String name) {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(getApplicationContext(), MainActivity.class);
        shortcutIntent.setAction(Intent.ACTION_RUN);

        // Decorate the shortcut
        Intent delIntent = new Intent();
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);

        // Inform launcher to remove shortcut
        delIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(delIntent);
    }
}
