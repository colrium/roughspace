<?php
//check if any data has been posted
		$postedData = $this->input->post(NULL, FALSE);

		if (sizeof($postedData)>0) {
			$newrecId = addupdatedbtablerecord('system_prefs', $postedData, 1, FALSE);
			if (!array_key_exists('strerror', $this->displayData)) {
				   	setflashnotifacation('message', array('icon'=>'block', 'alert'=>'Sorry. Access has been denied. You dont have Administrative priviledges')); 
		            preredirect('preferences/System/preferences', 'refresh');
			}
		}

			$this->displayData['title']        = 'System Preferences';
            $this->displayData['prefs']        = dbtablerecords('system_prefs', array(), FALSE);
            $this->displayData['pageTitle']    = ' '.CENUM_CHIP_POINTER.'  '.breadcrumb('System Preferences');
            $this->displayData['mainTemplate'] = 'preferences/systempreferences';

            renderpage($this->displayData);  