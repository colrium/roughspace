
$(function(){
	$("body").delauneybg({
		colors:["#f5f5f5", "#e0e0e0"]
	});
});
$.get($.fn.site_url("api/Api/userpriveledges"), { "module": priveledgemodule, "priviledge": priviledge }).done(function(data) {			
			if ($.isFunction(callback)) {
				callback.call(this, data);
			}
		});


 //set highchart colors   
$(function() {

	var monochromecolors = function (colorindex) {
		var colors = [],
			base = Highcharts.getOptions().colors[colorindex],
			i;

		for (i = 0; i < 10; i += 1) {
			// Start out with a darkened base color (negative brighten), and end
			// up with a much brighter color
			colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
		}
		return colors;
	};
	

//Entity donations aggregate highcharts piechart

	$('#entitesaggregates').highcharts({
		chart: {
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			}
		},
		title: {
			text: ''
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,
				dataLabels: {
					enabled: false
				},
				showInLegend: true,
				colors: monochromecolors(2)
			}
		},
		series: [{
			type: 'pie',
			name: 'Share',
			data: []
		}]
	});


//status chart


 $('#organisationstatus').highcharts({
		chart: {
			type: 'bar'
		},
		title: {
			text: ""
		},
		subtitle: {
			text: 'As at <?php echo date('l, F dS Y ');?>'
		},
		xAxis: {
			categories: ['Income'],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Totals (KES)',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' (KES)'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [{
			name: 'Expected',
			data: []
		},
		{
			name: 'Realized',
			data: []
		}]
	});









$('#attrtimeline').highcharts({
		title: {
			text: '',
			x: -20 //center
		},
		
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'Total People/organisations'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			valueSuffix: 'Total'
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle',
			borderWidth: 0
		},
		series: []
	});



$('#attrmonthlytotals').highcharts({
		title: {
			text: '',
			x: -20 //center
		},
		
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'Donations Totals'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			valueSuffix: 'KES'
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle',
			borderWidth: 0
		},
		series: []
	});


});




















